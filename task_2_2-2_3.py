import csv

coordinate = [

]
with open("coordinate.csv", mode="w") as w_file:
    coor = ["x", "y"]
    file_writer = csv.DictWriter(w_file, delimiter = ",", 
                                 lineterminator="\r", fieldnames=coor)
    file_writer.writeheader()
    file_writer.writerow({"x": "1", "y": "1"})
    file_writer.writerow({"x": "2", "y": "3"})
    file_writer.writerow({"x": "4", "y": "5"})
    file_writer.writerow({"x": "5", "y": "5"})
    file_writer.writerow({"x": "5", "y": "4"})
    file_writer.writerow({"x": "5", "y": "2"})
    file_writer.writerow({"x": "4", "y": "1"})
    file_writer.writerow({"x": "5", "y": "1"})
    file_writer.writerow({"x": "3", "y": "1"})
    file_writer.writerow({"x": "1", "y": "1"})

