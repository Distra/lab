import matplotlib.pyplot as mp
import csv

class chart:
    x = []
    y = []

    def __init__(self, x, y):
        self.x = x
        self.y = y

    def Center(self):
        x0 = list(map(int, self.x))
        y0 = list(map(int, self.y))

        x0.append(x0[0])
        y0.append(y0[0])

        ax = mp.gca()
        ax.spines['left'].set_position('center')
        ax.spines['bottom'].set_position('center')
        ax.spines['top'].set_visible(False)
        ax.spines['right'].set_visible(False)

        mp.plot(x0, y0)
        mp.show()

x0=[]
y0=[]

with open("coordinate.csv", "r", newline="") as file:
    reader = csv.reader(file)
    for row in reader:
        print(row[0], " - ", row[1])
        x0.append(row[0])
        y0.append(row[1])

x0.remove("x")
y0.remove("y")

x0[6] = 6
y0[6] = 2

object = chart(x0, y0)
object.Center()